import cv2
vc = cv2.VideoCapture(0)

while True:
		rval, image = vc.read()
		image = cv2.GaussianBlur(image, (5, 5), 0)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		_, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
		contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
		
		for contour in contours:
			area = cv2.contourArea(contour)
			
			if area < 25000 or area > 30000:
				continue
				
			approx = cv2.approxPolyDP(contour, 0.002*cv2.arcLength(contour, True), True)
			print(approx)
			cv2.drawContours(image, [approx], -1, (0,0,255), 1)
				
			moments = cv2.moments(contour)
			cx = int(moments["m10"] / moments["m00"])
			cy = int(moments["m01"] / moments["m00"])
				
			cv2.circle(image, (cx, cy), 7, (255, 255, 255), -1)
				
		print("----")
		#cv2.drawContours(image, contours, -1, (0,0,255), 2)
		#for contour in contours:
		#	perimeter = cv2.arcLength(contour, True)
		#	approx = cv2.approxPolyDP(contour, 0.04 * perimeter,True)
		#	epsilon = 0.0000000002 * cv2.arcLength(contour, True)
		#	approx = cv2.approxPolyDP(cnt,epsilon, True)
				
		#	if len(approx) ==4:
		#		cv2.drawContours(image, contour,0,(0,0,255),20)
		cv2.imshow("Window", image)
		cv2.imshow("window_2", thresh)
		key = cv2.waitKey(6)
		if key == 27:
			break
